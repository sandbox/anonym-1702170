
-- SUMMARY --

Module provides a new formatter for "taxonomy term reference"
fields to view term fields into nodes (entities).

-- REQUIREMENTS --

* Taxonomy module.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Select "Entity" formatter for your taxonomy term reference field.
* Configure view mode "Entity" for taxonomy vocabulary.
